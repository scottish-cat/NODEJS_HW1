const fs = require('fs');

const logsPath = './file/logs.json';

module.exports = {
    logMessage: (message) => {
        if (fs.existsSync(logsPath)) {
            fs.readFile(logsPath, (error, content) => {
                if (error) throw error;
                const logs = !content.toString() ? { logs: [] } : JSON.parse(content);
                logs.logs.push({ message, time: new Date().getTime() });
                fs.writeFile(logsPath, JSON.stringify(logs), 'utf8', (err) => {
                    if (err) throw err;
                });
            });
        } else {
            fs.writeFile(logsPath, JSON.stringify({ logs: [ { message, time: new Date().getTime() } ] }), 'utf8', (err) => {
                if (err) throw err;
            });
        }
    },
    getLogs: (fromDate, toDate) => {
        if (fs.existsSync(logsPath)) {
            const content = fs.readFileSync(logsPath)
            fromDate = fromDate ? fromDate : 0;
            toDate = toDate ? toDate : new Date().getTime();
            const parsedLogs = !content.toString() ? { logs: [] } : JSON.parse(content);
            return JSON.stringify({ logs: parsedLogs.logs.filter(el => el.time >= fromDate && el.time <= toDate) });
        } else {
            return 'No logs are available.';
        }
    } 
};