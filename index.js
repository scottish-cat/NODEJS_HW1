const http = require('http');
const fs = require('fs');
const url = require('url'); 

const logger = require('./logger');

const handleResponse = (response, statusCode, contentType = '', content = '') => {
    response.writeHead(statusCode, contentType ? { 'Content-type': contentType } : contentType);
    response.end(content);
}

const validateParams = (filename) => {
    return filename && filename.match(/^[\w\-. ]+$/);

}

module.exports = () => {
    http.createServer(function(request, response) {
        const { pathname, query } = url.parse(request.url, true);
        if (request.method === 'POST') {
            const { filename, content } = query;
            logger.logMessage(`New file '${filename}' was saved.`);
            if (validateParams(filename, content)) {
                fs.writeFile(`./${pathname}/${filename}`, content ? content : '', 'utf8', (err) => {
                    if (err) {
                        logger.logMessage(`Error while creating '${pathname}': not valid path/name.`);
                        handleResponse(response, 400, 'text/html', `File with such path/name '${pathname}' couldn't be created.`);
                    } else {
                        handleResponse(response, 200, 'text/html');
                    } 
                });
            } else {
                logger.logMessage(`Error while creating '${pathname}': not valid path/name.`);
                handleResponse(response, 400, 'text/html', `File with such path/name '${pathname}' couldn't be created.`);
            }

        } else if (request.method === 'GET') {
            if (pathname === '/logs') {
                content = logger.getLogs(query.from, query.to);
                handleResponse(response, 200, 'text/json', content);
                return;
            }
            
            fs.readFile(`./${pathname}`, (err, content) => {
                if (err) {
                    logger.logMessage(`Error while reading '${pathname}': file doesn't exist.`);
                    handleResponse(response, 400, 'text/html', `No file with such path '${pathname}' was found.`);
                } else {
                    logger.logMessage(`File '${pathname}' was read.`);
                    handleResponse(response, 200, 'text/html', content);
                }
            });
        } else {
            handleResponse(response, 405);
        }
    }).listen(process.env.PORT || 8080);
}
